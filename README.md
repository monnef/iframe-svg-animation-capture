iframe-svg-animation-capture
===

This is a tool for converting "iframe SVG JavaScript animation" to a series of SVG files - snapshots/frames.

Tested with files created by: **Aphalina - HTML5 Animation Maker** (aka Aphalina Designer, Aphalina Animator)

Live version: https://monnef.gitlab.io/iframe-svg-animation-capture/

Development
---

Install dependencies:

```
npm i
```

***

For dev mode use:

```
npm start
```

***

To build a production version:

```
npm run build
```

Result will be located at `/dist`.
