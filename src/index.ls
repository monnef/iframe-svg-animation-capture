require! {
  jquery: $
  jszip
  lodash: _
  'file-saver'
  bootstrap
}

#require 'bootstrap/dist/css/bootstrap.css'
require './bootstrap-workaround.scss'
require './index.styl'

srcHtml = $ '#input-html'
anim = $ '#anim'
processButton = $ '#process'
saveButton = $ '#save'
generatedText = $ '#generated-text'

data =
  iframeDoc: null
  captured: []
  capturing: false
  saveFn: ->

getDelay = -> $ '#delay' .val!
getFrameCount = -> $ '#frame-count' .val!
getFrameDelay = -> $ '#frame-delay' .val!
getStatus = -> $ '#status'

setStatus = (x) !-> getStatus! .text x

showDownloadDialog = !-> data.saveFn!

doCapture = !->
  setStatus "Capturing frame #{data.captured.length}"
  snapshot = data.iframeDoc.find \svg .parent! .html!
  data.captured.push snapshot
  if data.captured.length < getFrameCount!
    setStatus "Captured frame #{data.captured.length}, waiting"
    setTimeout doCapture, getFrameDelay!
  else
    setStatus 'Finished capturing, packaging'
    console.log 'result:', data.captured
    zip = new jszip
    for x, i in data.captured
      zip.file 'frame' + _.padStart(i, 4, '0') + \.svg, x
    zip.generateAsync({type : "blob"}).then (blob) !->
      setStatus 'Packaging finished, triggering download dialog.'
      saveFn = -> fileSaver.saveAs blob, \animation.zip
      data.saveFn = saveFn
      saveButton .prop \disabled false
      generatedText .show!
      showDownloadDialog!

startCapture = !->
  unless data.capturing
    setStatus 'Starting capturing'
    console.log 'startCapture'
    doCapture!

processButton .click !->
  setStatus 'Setting up'
  processButton .prop \disabled true
  anim.empty!
  doc = anim[0].contentDocument
  doc.write srcHtml.val!
  setTimeout ->
    setStatus 'Trigerring iframe\'s on load handler'
    doc.body?.onload?!
    data.iframeDoc = $ doc
    startCapture!
  , getDelay!

saveButton .click !->
  data.saveFn!

init = !->
  setStatus 'Idle'
  saveButton .prop \disabled true
  generatedText .hide!

init!
